import { Injectable } from "@angular/core";

@Injectable()
export class CounterService {
    activeCounter = 0;
    inactiveCounter = 0;

    onActiveCounterUpdated() {
        this.activeCounter++;
        console.log("Inactive counter " + this.activeCounter);
    }

    onInactiveCounterUpdated() {
        this.inactiveCounter++;
        console.log("Inactive counter: " + this.inactiveCounter);
    }
}